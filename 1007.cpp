#include <iostream>
#include <map>
using namespace std;

std::map<int, string> dispatch;
int calc(const char *dna, int len);

int main(int argc, char *argv[])
{
    int len, n, i;
    cin >> len >> n;
    string dna;
    for (i = 0; i < n; i++) {
        cin >> dna;
        dispatch.insert(std::map<int, string>::value_type(calc(dna.c_str(), len), dna));
    }
    std::map<int, string>::iterator it;
    for (it = dispatch.begin(); it != dispatch.end(); it++) {
        std::cout << it->second << std::endl;
    }
    return 0;
}

int calc(const char *dna, int len)
{
    int cnt = 0;
    int i, j;
    for (i = 0; i < len - 1; i++) {
        for (j = i + 1; j < len; j++) {
            if (toupper(dna[i]) > toupper(dna[j])) {
                cnt++;
            }
        }
    }
    return cnt;
}