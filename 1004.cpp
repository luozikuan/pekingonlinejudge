#include <iostream>
#include <iomanip>
using namespace std;

int main(int argc, char *argv[])
{
    double tmp = 0.00;
    double total = 0.00;
    int i;
    for (i = 0; i < 12; i++) {
        cin >> tmp;
        total += tmp;
    }
    std::cout<< '$' << setiosflags(ios::fixed) << setprecision(2) << total / 12 << std::endl;
    return 0;
}