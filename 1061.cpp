#include <iostream>
#include <stdlib.h>
using namespace std;

int main(int argc, char *argv[])
{
    int x, y, m, n, L;
    cin>>x>>y>>m>>n>>L;
    int cnt = 0;

    while (x % L != y % L) {
        x += m;
        y += n;
        cnt++;
    }
    cout<<cnt<<endl;
    // when "Impossible"?
    return 0;
}