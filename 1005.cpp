#include <iostream>
using namespace std;
int main()
{
    int i=0,n;
    double x, y;
    cin >> n;
    while(++i<=n){
        cin >> x >> y;
        cout << "Property "<< i <<": This property will begin eroding in year "<<(int)(3.1415926*(x*x+y*y)/100+1)<<".\n";
    }
    cout<<"END OF OUTPUT.\n";
    return 0;
}
