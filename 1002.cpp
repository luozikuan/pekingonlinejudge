#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <cstring>
using namespace std;

char table[129] =
    "                                                " // 00-47
    "0123456789"                                       // 48-57
    "       "                                          // 58-64
    "2223334445556667 77888999 "                       // 65-90
    "      "                                           // 91-96
    "2223334445556667 77888999 "                       // 97-122
    "    \0";                                          // 123-128

int comp(const void *a, const void *b)
{
    return *(int*)a - *(int*)b;
}

int main(int argc, char *argv[])
{
    int i, n;
    cin>>n;
    cin.ignore();
    int numbers[n];
    char c;
    int num;

    for (i = 0; i < n; i++) {
        num = 0;
        while ((c = getchar()) != '\n') {
            if (c != '-') {
                num = num * 10 + table[(int)c] - '0';
            }
        }
        numbers[i] = num;
    }
    qsort(numbers, n, sizeof(int), comp);
    int cnt = 1;
    bool dup = false;
    for (i = 0; i < n - 1; i++) {
        if (numbers[i] == numbers[i+1]) {
            cnt++;
            dup = true;
        } else if (cnt != 1) {
            printf ("%03d-%04d %d\n",
                numbers[i] / 10000,
                numbers[i] % 10000,
                cnt);
            cnt = 1;
        }
    }
    if (cnt != 1) {
        printf ("%03d-%04d %d\n",
            numbers[i] / 10000,
            numbers[i] % 10000,
            cnt);
    }
    if (!dup) {
        cout<<"No duplicates."<<endl;
    }
    return 0;
}
