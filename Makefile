CC = clang++
CCFLAGS = -g -Wall
T = 

my_app : clean
	$(CC) $(CCFLAGS) $(T).cpp -o $@

.PHONY : clean
clean:
	-$(RM) $(OBJS) *~ *.d *.o my_app
